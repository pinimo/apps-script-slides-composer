// Here, we centralize functions to fetch Drive objects, and Sheets 
// components, such as ranges we use in the Sheet.

function getActiveSpreadsheet() {
  return SpreadsheetApp.getActive()
}

function getOutputNameRange(spreadSheet) {
  return spreadSheet.getRangeByName("OutputName")
}

function getSongIdsRange(spreadSheet) {
  return spreadSheet.getRangeByName("SongIds")
}

function getSongNamesRange(spreadSheet) {
  return spreadSheet.getRangeByName("SongNames")
}

function getResultDisplayRange(spreadSheet) {
  return spreadSheet.getRangeByName("ResultDisplay")
}

function getEggPresentation() {
  return DriveApp.getFileById(config.eggPresentation)
}

function getSongsFolder() {
  return DriveApp.getFolderById(config.songsFolder)
}

function getOutputFolder() {
  return DriveApp.getFolderById(config.outputFolder)
}

function getSongsSheet(activeSpreadsheet) {
  return activeSpreadsheet.getSheetByName("config - chants")
}