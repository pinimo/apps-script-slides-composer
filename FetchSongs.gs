function fetchSongs() {
  var sheet = getSongsSheet(getActiveSpreadsheet());
  var folder = getSongsFolder();
  var contents = folder.getFiles();

  sheet.clearContents()
  sheet.appendRow(["Name", "FileId", "Feuille créée automatiquement, ne pas modifier manuellement.\nUtiliser le bouton \"Actualiser la liste des chants\""]);

  var cnt = 0;
  var file;

  while (contents.hasNext()) {
    var file = contents.next();
      cnt++;
        data = [
          file.getName(),
          file.getId(),
        ];
        sheet.appendRow(data);
    };
};