

function generateSlides() {
  // Read parameters in the program "Google Sheets" sheet
  activeSpreadsheet = getActiveSpreadsheet()

  outputName = getOutputNameRange(activeSpreadsheet).getValue()
  outputFolder = getOutputFolder()
  songIDs = [].concat.apply([], getSongIdsRange(activeSpreadsheet).getValues()).filter(a => a != "")
  resultDisplayRange = getResultDisplayRange(activeSpreadsheet)
  resultDisplayRange.setValue("Génération en cours, merci de patienter...")
  resultDisplayRange.setBackground('grey20')
  SpreadsheetApp.flush() // otherwise the "Please wait" message does not appear

  Logger.log("outputName = " + outputName)
  Logger.log("songIDs = " + songIDs)

  // Function to insert a black slide at the end of `presentation`.
  // It is called before inserting the first song and after 
  // inserting each song.
  blackSlide = null
  function appendBlackSlide(presentation) {
    if (blackSlide == null) {
      Logger.log("Creating black slide")
      blackSlide = presentation.appendSlide()
      blackRectangle = blackSlide.insertShape(
        SlidesApp.ShapeType.RECTANGLE, 
        -2, // make the black rectangle a bit larger so we don't see its borders
        -2, 
        presentation.getPageWidth() + 4,
        presentation.getPageHeight() + 4
      )
      blackRectangle.getFill().setSolidFill(0, 0, 0)
    } else {
      Logger.log("Reusing black slide")
      presentation.appendSlide(blackSlide)
    }
    return presentation
  }

  // Copy "egg presentation" with predefined style, page format.
  // It is a blank presentation with no slides.
  resultPresentationId = getEggPresentation()
    .makeCopy(outputName, outputFolder)
    .getId();
  resultPresentation = SlidesApp.openById(resultPresentationId)

  resultPresentation = appendBlackSlide(resultPresentation)
  songIDs.forEach(function(songID) {
    Logger.log("songID = " + songID)
    songPresentation = SlidesApp.openById(songID)
    Logger.log("Opened slide " + songID)
    songPresentation.getSlides().forEach(function(songSlide) {
      resultPresentation.appendSlide(songSlide)
    })
    resultPresentation = appendBlackSlide(resultPresentation)
  })

  resultPresentationDrive = DriveApp.getFileById(resultPresentation.getId())
  resultPresentationDrive.moveTo(outputFolder)
  resultPresentationUrl = resultPresentationDrive.getUrl()
  resultDisplayRange.setValue(resultPresentationUrl)
}