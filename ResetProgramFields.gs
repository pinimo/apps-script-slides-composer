function resetProgramFields() {
  function isCellValueResettable(cellValue) {
    /// If the song name starts with "AAA" we do not want to reset / clear it.
    return !cellValue.startsWith("AAA")
  }
  function emptyResettableCells(valuesArray) {
    res = valuesArray.map(
      row => row.map(
        v => isCellValueResettable(v) ? "" : v
      )
    )
    Logger.log("valuesArray = " + valuesArray + " of length " + valuesArray.length)
    Logger.log("res = " + res + " of length " + res.length)
    return res
  }
  programSheet = getActiveSpreadsheet()
  rangesToReset = [
    getSongNamesRange(programSheet), 
    getOutputNameRange(programSheet),
    getResultDisplayRange(programSheet),
  ]
  rangesToReset.forEach(function (range) {
    beforeValues = range.getValues()
    afterValues = emptyResettableCells(beforeValues)
    range.setValues(afterValues)
  })
}